// import axios from 'axios';
// import store from '../store/index';

export default {
  isTokenInvalid() {
    const tokenInDb = JSON.parse(localStorage.getItem('token'));
    // let isNotValid = true;

    if (tokenInDb) {
      // todo a modifier si nous avons le temps
      // axios.get('/api/Profils',
      //   {
      //     headers: {
      //       Authorization: `bearer ${tokenInDb.access_token}`,
      //     },
      //   }).then((response) => {
      //     isNotValid = false;
      //     console.log(`reponse.status ${response.status}`);
      //     console.log(`reponse.status ${response.data}`);
      //     return isNotValid;
      //   }).catch((error) => {
      //     isNotValid = true;
      //     console.log(`error.status ${error.status}`);
      //     console.log(`error.data ${error.data}`);
      //     return isNotValid;
      //   });
      return false;
    }
    return true;
  },

  tokenIsInvalid() {
    const tokenInDb = JSON.parse(localStorage.getItem('token'));

    if (tokenInDb) {
      const now = new Date();
      // const utcTimestamp = Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
      //   now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
      const dateInDB = new Date(parseInt(tokenInDb.expire_in));
      if (parseInt(dateInDB.getHours()) >= parseInt(now.getHours())) {
        return !true;
      }
      return !false;
    }
    return !false;
  },

  getEmail() {
    return JSON.parse(localStorage.getItem('user')).Email;
  },

  deconnectUser() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  },
};

import userModelEmpty from './models/user';

// export const STORAGE_KEY = 'gestprog-storage-key';
//
// // for testing
// if (navigator.userAgent.indexOf('PhantomJS') > -1) {
//   window.localStorage.clear();
// }
//
// export const state = {
//   todos: JSON.parse(window.localStorage.getItem(STORAGE_KEY) || '[]'),
// };

// mutation is what we can do
export default {
  addToken(state, { token }) {
    state.localStorageObjects.push({
      token,
      done: false,
    });
  },

  deleteToken(state, { token }) {
    state.localStorageObjects.splice(state.localStorageObjects.indexOf(token), 1);
  },

  addUser(state, { user }) {
    state.user = user;
  },

  removeUser(state) {
    state.user = userModelEmpty;
  },

  // toggleTodo(state, { token }) {
  //   token.done = !token.done;
  // },
  //
  // editTodo(state, { token, value }) {
  //   token.text = value;
  // },
  //
  // toggleAll(state, { token }) {
  //   state.localStorageObjects.forEach((token) => {
  //     token.done = done;
  //   });
  // },

  clearCompleted(state) {
    state.localStorageObjects = state.localStorageObjects.filter(todo => !todo.done);
  },
};
